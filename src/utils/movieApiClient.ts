import {
  ApiError,
  ApiResponse,
  FullMovieResponse,
  Movie,
  MovieReview,
} from "./typesApi";
import placeHolder from "../assets/movie-placeholder.png";

const apiKey = "affc0edf3f789f9357f1d525ba2cdd23";
const apiUrl = "https://api.themoviedb.org/3";
class ApiClient {
  private apiKey: string;
  public apiUrl: string;
  private bigImageUrl = "https://image.tmdb.org/t/p/w600_and_h900_bestv2";
  private smallImageUrl = "https://image.tmdb.org/t/p/w200";

  constructor(apiKey: string, apiUrl: string) {
    this.apiKey = apiKey;
    this.apiUrl = apiUrl;
  }

  buildMoviePosterUrl(relativeUrl: string, small = false): string {
    if (!relativeUrl) return placeHolder;
    if (small) {
      return `${this.smallImageUrl}${relativeUrl}`;
    } else {
      return `${this.bigImageUrl}${relativeUrl}`;
    }
  }

  async getMovieDetail(movieId: string): Promise<FullMovieResponse | ApiError> {
    try {
      const response = await fetch(
        `${apiUrl}/movie/${movieId}?api_key=${this.apiKey}`,
        {
          headers: {
            "Content-type": "application/json",
          },
        }
      );
      const data: FullMovieResponse = await response.json();
      return data;
    } catch (err) {
      console.error(err);
      return {
        message: "An error has ocurred while fetching data",
      } as ApiError;
    }
  }

  async getMovieList(
    searchText = "star wars",
    currentPage = 1
  ): Promise<ApiResponse<Movie> | ApiError> {
    try {
      const response = await fetch(
        `${apiUrl}/search/movie?query=${searchText}&page=${currentPage}&api_key=${this.apiKey}`,
        {
          headers: {
            "Content-type": "application/json",
          },
        }
      );
      const data: ApiResponse<Movie> = await response.json();

      return data;
    } catch (err) {
      console.error(err);
      return {
        message: "An error has ocurred while fetching data",
      } as ApiError;
    }
  }

  async getMovieReviewList(
    movieId: string
  ): Promise<ApiResponse<MovieReview> | ApiError> {
    try {
      const response = await fetch(
        `${apiUrl}/movie/${movieId}/reviews?api_key=${this.apiKey}`,
        {
          headers: {
            "Content-type": "application/json",
          },
        }
      );
      const data: ApiResponse<MovieReview> = await response.json();
      return data;
    } catch (err) {
      console.error(err);
      return {
        message: "An error has ocurred while fetching data",
      } as ApiError;
    }
  }

  async getMovieListNowPlaying(): Promise<ApiResponse<Movie> | ApiError> {
    try {
      const response = await fetch(
        `${apiUrl}/movie/now_playing?api_key=${this.apiKey}`,
        {
          headers: {
            "Content-type": "application/json",
          },
        }
      );
      const data: ApiResponse<Movie> = await response.json();
      return data;
    } catch (err) {
      console.error(err);
      return {
        message: "An error has ocurred while fetching data",
      } as ApiError;
    }
  }

  async getMovieListUpcoming(): Promise<ApiResponse<Movie> | ApiError> {
    try {
      const response = await fetch(
        `${apiUrl}/movie/upcoming?api_key=${this.apiKey}`,
        {
          headers: {
            "Content-type": "application/json",
          },
        }
      );
      const data: ApiResponse<Movie> = await response.json();
      return data;
    } catch (err) {
      console.error(err);
      return {
        message: "An error has ocurred while fetching data",
      } as ApiError;
    }
  }
}

// The Singleton Pattern (Api Client, Db Client)
export default new ApiClient(apiKey, apiUrl);
