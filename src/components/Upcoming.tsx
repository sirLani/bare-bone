import React, { useEffect, useState } from "react";
import movieApiClient from "../utils/movieApiClient";
import { ApiError, isApiError, Movie } from "../utils/typesApi";
import MovieListCardDisplay from "./MovieCardListDisplay";
import MovieListCardDisplayLoader from "./MovieCardListDisplayLoader";

export default function Upcoming() {
  const [movieListUpcoming, setMovieListUpcoming] = useState<Movie[] | null>();
  const [error, setFetchError] = useState<ApiError | null>();
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    setIsLoading(true);
    movieApiClient.getMovieListUpcoming().then((data) => {
      if (isApiError(data)) {
        setFetchError(data);
        setIsLoading(false);
      } else {
        setMovieListUpcoming(data.results);
        setIsLoading(false);
      }
    });
  }, []);

  return (
    <>
      {isLoading ? (
        <MovieListCardDisplayLoader headingText={"Upcoming Now"} />
      ) : (
        <MovieListCardDisplay
          movieList={movieListUpcoming}
          headingText={"Upcoming Now"}
          error={error}
        />
      )}
    </>
  );
}
