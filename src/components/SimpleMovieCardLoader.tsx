import React, { memo } from "react";
import styled from "styled-components";

function SimpleMovieCardLoader() {
  return (
    <SimpleMovieCardContainer>
      <LoaderPlaceholder />
    </SimpleMovieCardContainer>
  );
}

// HOC wrapping the original component
export default memo(SimpleMovieCardLoader);

const LoaderPlaceholder = styled.div`
  height: 172px;
  width: 113px;
  border: solid 1px grey;
`;

const SimpleMovieCardContainer = styled.div`
  margin-left: 1px;
  margin-right: 1px;
  margin-bottom: 1px;
`;
