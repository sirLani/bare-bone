import React, { useContext } from "react";
import styled from "styled-components";
import { DarkModeContext } from "../store/context";
import MovieCardContainer from "./styled/MovieCardContainer";
import placeHolder from "../assets/movie-placeholder.png";

const MovieCardLoader = () => {
  const context = useContext(DarkModeContext);
  return (
    <MovieCardContainer data-testid={`movie-card-container-loading`}>
      <img width="159" height="238" alt="movie-poster" src={placeHolder}></img>
      <MovieCardSummary>
        <MovieTitle
          data-testid={`movie-card-title-id`}
          color={context.theme.foreground}
        >
          {"Loading..."}
        </MovieTitle>

        <MovieDate color={context.theme.foreground}>
          Release Date: ______
        </MovieDate>
        <MoviePlot color={context.theme.foreground}>
          Plot: {"Loading..."}
        </MoviePlot>
      </MovieCardSummary>
    </MovieCardContainer>
  );
};

export default MovieCardLoader;

const MovieCardSummary = styled.div`
  padding: 10px;
  text-decoration: none;
  color: ${(props) => props.color};
`;

const MovieTitle = styled.h2`
  color: ${(props) => props.color};
`;

const MoviePlot = styled.p`
  color: ${(props) => props.color};
  text-decoration: none;
  height: 100px;
  overflow: hidden;
  font-size: 0.8em;
  // white-space: nowrap;
  text-overflow: ellipsis;
`;

const MovieDate = styled.p`
  color: ${(props) => props.color};
`;
