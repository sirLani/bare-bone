import React, { memo, useEffect, useState } from "react";
import movieApiClient from "../utils/movieApiClient";
import { ApiError, isApiError, Movie } from "../utils/typesApi";
import MovieListCardDisplay from "./MovieCardListDisplay";
import MovieListCardDisplayLoader from "./MovieCardListDisplayLoader";

function TrendingNow() {
  const [movieListTrending, setMovieListTrending] = useState<Movie[] | null>();
  const [error, setFetchError] = useState<ApiError | null>();
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    setIsLoading(true);
    movieApiClient.getMovieListNowPlaying().then((data) => {
      if (isApiError(data)) {
        setFetchError(data);
        setIsLoading(false);
      } else {
        setMovieListTrending(data.results);
        setIsLoading(false);
      }
    });
  }, []);

  return (
    <>
      {isLoading ? (
        <MovieListCardDisplayLoader headingText={"Trending Now"} />
      ) : (
        <MovieListCardDisplay
          movieList={movieListTrending}
          headingText={"Trending Now"}
          error={error}
        />
      )}
    </>
  );
}

export default memo(TrendingNow);
