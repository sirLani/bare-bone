import React, {
  Dispatch,
  SetStateAction,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import movieApiClient from "../utils/movieApiClient";
import { Movie } from "../utils/typesApi";
import MovieCardContainer from "./styled/MovieCardContainer";
import { DarkModeContext } from "../store/context";

interface MovieCardProps {
  movie: Movie;
}

export default function MovieCard({ movie }: MovieCardProps) {
  const navigate = useNavigate();
  const context = useContext(DarkModeContext);
  const [shortPlot, setShortPlot] = useState(movie.overview);

  const onCardClick = () => {
    navigate(`/movie/${movie.id}`);
  };

  useEffect(() => {
    plotShorten(movie.overview, 250, setShortPlot);
  }, [movie.overview, setShortPlot]);

  const imgSrc = useMemo(() => {
    return movieApiClient.buildMoviePosterUrl(movie.poster_path, true);
  }, [movie.poster_path]);

  const releaseDate = useMemo(() => {
    if (!movie || !movie.release_date) return null;
    return Intl.DateTimeFormat().format(new Date(movie.release_date));
  }, [movie]);

  return (
    <MovieCardContainer
      data-testid={`movie-card-container-${movie.id}`}
      onClick={onCardClick}
    >
      <img width="159" height="238" alt="movie-poster" src={imgSrc}></img>
      <MovieCardSummary>
        <MovieTitle
          data-testid={`movie-card-title-${movie.id}`}
          color={context.theme.foreground}
        >
          {movie.title}
        </MovieTitle>

        <MovieDate color={context.theme.foreground}>
          Release Date: {releaseDate}
        </MovieDate>
        <MoviePlot color={context.theme.foreground}>
          Plot: {shortPlot}
        </MoviePlot>
      </MovieCardSummary>
    </MovieCardContainer>
  );
}

function plotShorten(
  text: string,
  length = 250,
  setShortPlot: Dispatch<SetStateAction<string>>
) {
  return import("lodash").then((_) => {
    // dynamic import
    const shortText = _.default.take(text.split(""), length).join("");
    setShortPlot(shortText + "...");
  });
}

const MovieCardSummary = styled.div`
  padding: 10px;
  text-decoration: none;
  color: ${(props) => props.color};
`;

const MovieTitle = styled.h2`
  color: ${(props) => props.color};
`;

const MoviePlot = styled.p`
  color: ${(props) => props.color};
  text-decoration: none;
  height: 100px;
  overflow: hidden;
  font-size: 0.8em;
  // white-space: nowrap;
  text-overflow: ellipsis;
`;

const MovieDate = styled.p`
  color: ${(props) => props.color};
`;
