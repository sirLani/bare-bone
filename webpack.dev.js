const { merge } = require("webpack-merge");
const common = require("./webpack.common.js");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = merge(common, {
  mode: "development",
  devtool: "inline-source-map",
  devServer: {
    port: 5001,
    historyApiFallback: true,
    static: {
      publicPath: "/",
    },
  },
  plugins: [new MiniCssExtractPlugin()],
});
